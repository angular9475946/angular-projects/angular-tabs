import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Tabbed App';
  tabs = [
    {title: 'Tab 1', content: 'Content for Tab 1'},
    {title: 'Tab 2', content: 'Content for Tab 2'},
    {title: 'Tab 3', content: 'Content for Tab 3'}
  ];
  activeTab = this.tabs[0];

  setActiveTab(tab: { title: string; content: string; }) {
    this.activeTab = tab;
  }
}
